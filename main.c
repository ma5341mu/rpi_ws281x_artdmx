// https://github.com/jgarff/rpi_ws281x
// https://www.geeksforgeeks.org/udp-server-client-implementation-c/
// https://en.wikipedia.org/wiki/Art-Net

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <signal.h>
#include <stdarg.h>
#include <getopt.h>

#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <netinet/in.h> 

#include "clk.h"
#include "gpio.h"
#include "dma.h"
#include "pwm.h"

#include "ws2811.h"
#include <pthread.h>

// defaults for cmdline options
#define TARGET_FREQ                 WS2811_TARGET_FREQ
#define GPIO_PIN                    18
#define DMA                         10
#define STRIP_TYPE                  WS2811_STRIP_RGB

#define DATA_OFFSET                 18      // Index in buffer where LED-Data starts
#define POSITION_OF_UNIVERSEINDEX   14      // Index in buffer where UniverseIndex is stored

#define LED_COUNT                   820     // Total number of LEDs to control
#define NUMBER_OF_UNIVERSES         10      // Total number of Universes
#define LEDS_PER_UNIVERSE           (LED_COUNT / NUMBER_OF_UNIVERSES)

#define BUFSIZE                     1500    // Size of buffer for received packet
#define PORT                        6454    //

#define CLEAR_ON_EXIT               1       // 1 --> Turn LEDs off on Exit


// -------------------------------------------
// Vars
// -------------------------------------------
ws2811_return_t ret;

static uint8_t running = 1;

ws2811_t sLedStrip =
{
    .freq = TARGET_FREQ,
    .dmanum = DMA,
    .channel =
    {
        [0] =
        {
            .gpionum = GPIO_PIN,
            .count = LED_COUNT,
            .invert = 0,
            .brightness = 255,
            .strip_type = STRIP_TYPE,
        },
    },
};

struct udp_t {
    int sockfd;
    char buffer[BUFSIZE]; 
    struct sockaddr_in servaddr; 
    struct sockaddr_in cliaddr;
    uint32_t packetLength;
    socklen_t len;
} sUdp;



// -------------------------------------------
// Functions
// -------------------------------------------

/**
 * Turn all LEDs off
 */
void clear_leds(void) {
    int i;
    for (i = 0; i < LED_COUNT; i++) 
        sLedStrip.channel[0].leds[i] = 0;
}

/**
 * Terminates program
 * @param signum
 */
static void ctrl_c_handler(int signum) {
        (void)(signum);
    running = 0;
}

/**
 * ¯\_(ツ)_/¯
 */
static void setup_handlers(void) {
    struct sigaction sa =
    {
        .sa_handler = ctrl_c_handler,
    };

    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGTERM, &sa, NULL);
}

/**
 * Checks if given packet _buffer is of type ArtDMX
 * @param _buffer 
 * @param _bufferLength
 * @return  0 --> not ArtDMX
 *          1 --> ArtDMX 
 */
uint32_t isArtnet(char* _buffer, uint32_t _bufferLength){
    if( _bufferLength <= 9 )
        return 0;
    
    if( _buffer[0] == 'A' &&
        _buffer[1] == 'r' &&
        _buffer[2] == 't' &&
        _buffer[3] == '-' &&
        _buffer[4] == 'N' &&
        _buffer[5] == 'e' &&
        _buffer[6] == 't' &&
        _buffer[9] == 'P' )
        return 1;
    
    return 0;
}
  
/**
 * Thread which controls LEDs
 * @return 
 */
void* controlLeds(){
    while(1){
        // control the leds
        if ((ret = ws2811_render(&sLedStrip)) != WS2811_SUCCESS) {
            fprintf(stderr, "ws2811_render failed: %s\n", ws2811_get_return_t_str(ret));
        }
        
        // 25fps
        usleep(1000000 / 25);
    }
    return NULL;
}

/**
 * init udp stuff
 */
void init_udp(){
      
    // Creating socket file descriptor 
    if ( (sUdp.sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) { 
        perror("socket creation failed"); 
        exit(EXIT_FAILURE); 
    } 
      
    memset(&sUdp.servaddr, 0, sizeof(sUdp.servaddr)); 
    memset(&sUdp.cliaddr, 0, sizeof(sUdp.cliaddr)); 
      
    // Filling server information 
    sUdp.servaddr.sin_family         = AF_INET;      // IPv4 
    sUdp.servaddr.sin_addr.s_addr    = INADDR_ANY; 
    sUdp.servaddr.sin_port           = htons(PORT); 
      
   
    // Bind the socket with the server address 
    if ( bind(sUdp.sockfd, (const struct sockaddr *)&sUdp.servaddr, sizeof(sUdp.servaddr)) < 0 ) { 
        perror("bind failed"); 
        exit(EXIT_FAILURE); 
    } 
}


int main()
{   
    // init rpi_ws281x
    setup_handlers();

    if ((ret = ws2811_init(&sLedStrip)) != WS2811_SUCCESS) {
        fprintf(stderr, "ws2811_init failed: %s\n", ws2811_get_return_t_str(ret));
        return ret;
    }
    
    // init UDP
    init_udp();
    
    // init and start Thread
    pthread_t thread_id;
    pthread_create(&thread_id, NULL, controlLeds, NULL);
    pthread_detach(thread_id);
    
    
    // main loop 
    while (running) {        
        
        // receive UDP-Packet
        sUdp.packetLength = recvfrom(sUdp.sockfd, (char*)sUdp.buffer, BUFSIZE, MSG_DONTWAIT, (struct sockaddr*) &sUdp.cliaddr, &sUdp.len); 
        
        // check if received packet is of type "ArtDMX" and the universe is within the configured range
        if(sUdp.buffer[POSITION_OF_UNIVERSEINDEX] <= NUMBER_OF_UNIVERSES && sUdp.buffer[POSITION_OF_UNIVERSEINDEX] >= 0 
                && isArtnet(sUdp.buffer, sUdp.packetLength)){
        
            uint32_t IndexInLedStrip = (sUdp.buffer[POSITION_OF_UNIVERSEINDEX] - 1) * LEDS_PER_UNIVERSE;
            
            // fill sLedStripArray with the data received via udp
            uint32_t bufferPosition;
            for(bufferPosition = DATA_OFFSET; bufferPosition < (DATA_OFFSET + 3 * LEDS_PER_UNIVERSE); bufferPosition += 3){

                /**
                 * Target Format: 0xWWRRGGBB
                 * sUdp.buffer[bufferPosition] == RR 
                 * sUdp.buffer[bufferPosition+1] == GG
                 * sUdp.buffer[bufferPosition+2] == BB
                 */
                
                // 0x0000RR
                sLedStrip.channel[0].leds[IndexInLedStrip] = (uint32_t)sUdp.buffer[bufferPosition];
                
                // 0x00RR00
                sLedStrip.channel[0].leds[IndexInLedStrip] = sLedStrip.channel[0].leds[IndexInLedStrip] << 8;
                
                // 0x00RRGG
                sLedStrip.channel[0].leds[IndexInLedStrip] |= (uint32_t)sUdp.buffer[bufferPosition + 1];
                
                // 0xRRGG00
                sLedStrip.channel[0].leds[IndexInLedStrip] = sLedStrip.channel[0].leds[IndexInLedStrip] << 8;  
                
                // 0xRRGGBB
                sLedStrip.channel[0].leds[IndexInLedStrip] |= (uint32_t)sUdp.buffer[bufferPosition + 2];
                IndexInLedStrip++;
            }
        }
    }

    if (CLEAR_ON_EXIT) {
	clear_leds();
	ws2811_render(&sLedStrip);
    }

    ws2811_fini(&sLedStrip);

    return ret;
}
